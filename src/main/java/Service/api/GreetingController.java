

package Service.api;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class GreetingController {

    @GetMapping(value = "/simple")
    public String simpleGreeting(
            @RequestParam(value = "name", required = true) String name,
            @RequestParam(value = "lastname", required = true) String lastName,
            Model model) {

        model.addAttribute("name2", name);
        model.addAttribute("lastname2", lastName);
        return "greeting.html";
    }
}